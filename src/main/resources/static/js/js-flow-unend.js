layui.use(['form','table'],function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    var dataTable = table.render({
        elem: '#table',
        url: '/flowUnfinish/queryFlowUnfinish',
        cols: [[
            {field: 'moduleid', title: '模块ID'},
            {field: 'modulename', title: '模块名称'},
            {field: 'processdefineid', title: '流程定义ID'},
            {field: 'processversion', title: '流程版本'},
            {field: 'processinstanceid', title: '流程实例ID'},
            {field: 'taskid', title: '流程节点ID'},
            {field: 'taskname', title: '流程节点名称'},
            {field: 'createtime',templet:'<div>{{ layui.util.toDateString(d.createTime, "yyyy-MM-dd HH:mm:ss") }}</div>', title: '任务创建时间'},
            {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit: 10,
        page: true,
        skin: 'line'
    });


    /**
     * 监听表格选择
     */
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'delete') {
        	layer.confirm('确定删除行么', function (index) {
                $.ajax({
                    beforeSend: function() {
                        layer.load(2);
                    },
                    type: 'POST',
                    url: '/flowUnfinish/deleteProcess',
                    data: data.processinstanceid,
                    contentType: "application/json",
                    dataType: 'json',
                    success: function (res) {
                        if (res.code == 200) {
                            layer.msg(res.msg, { icon: 1 ,time: 1000});
                            table.reload('table');
                        } else {
                            layer.msg(res.msg, {icon: 5, time: 2000});
                        }
                    },
                    complete: function() {
                        layer.closeAll("loading");
                    },
                    error: function() {
                        layer.msg('系统繁忙请稍后重试', {icon: 5, time: 2000});
                    }
                });
            });
        }
    });

});

