package com.sunny.activiti.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sunny.activiti.entity.FlowRule;

import io.lettuce.core.dynamic.annotation.Param;

/**
 * <p>
 * 流程规则表 Mapper 接口
 * </p>
 *
 * @author sunt
 * @since 2020-06-01
 */
public interface FlowRuleMapper extends BaseMapper<FlowRule> {
	
	/**
	 * 
	 * @return
	 */
	@Select("SELECT\n" +
			"			p.KEY_ moduleid,\n" +
			"			p.NAME_ modulename,\n" +
			"			p.ID_ processdefineid,\n" +
			"			p.VERSION_ processversion,\n" +
			"			t.PROC_INST_ID_ processinstanceid,\n" +
			"			t.EXECUTION_ID_ taskid,\n" +
			"			t.NAME_ taskname,\n" +
			"			t.CREATE_TIME_ createtime \n" +
			"  FROM act_ru_task t\n" +
			"  JOIN act_re_procdef p ON p.ID_ = t.PROC_DEF_ID_ \n" +
			"  LIMIT #{param1}, #{param2}")
	List<Map<String, Object>> queryFlowUnfinish(int pageSize, int pageNum);
	
	/**
	 * 
	 * @return
	 */
	@Select("SELECT count(0) \n" +
			"  FROM act_ru_task t\n" +
			"  JOIN act_re_procdef p ON p.ID_ = t.PROC_DEF_ID_")
	int queryFlowUnfinishCount();

}
