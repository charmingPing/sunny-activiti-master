package com.sunny.activiti.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunny.activiti.common.entity.PageBean;
import com.sunny.activiti.common.entity.ResponseResult;
import com.sunny.activiti.common.entity.ResponseTableResult;
import com.sunny.activiti.common.entity.ResponseUtil;
import com.sunny.activiti.service.IFlowInfoService;

/**
 * 
 * @author Administrator
 *
 */
@RestController
@RequestMapping("flowUnfinish")
public class FlowUnfinishController {
    
    @Autowired
    private IFlowInfoService flowInfoService;

    /**
     * 查询未结束的流程信息
     * @return
     */
    @SuppressWarnings("unchecked")
	@RequestMapping("queryFlowUnfinish")
    public ResponseTableResult<List<Map<String, Object>>> queryFlowUnfinish(PageBean pageBean) {
    	Map<String, Object> flowRulePage = flowInfoService.queryFlowUnfinish(pageBean);
        return ResponseUtil.makeTableRsp(0, (int) flowRulePage.get("count"), (List<Map<String, Object>>)flowRulePage.get("list"));
    }
    
	@RequestMapping("deleteProcess")
    public ResponseResult<String> deleteProcess(@RequestBody String key) {
        return ResponseUtil.makeOKRsp(flowInfoService.deleteProcess(key));
    }

}
