/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : test_db

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 16/08/2021 00:32:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for scheduled_task
-- ----------------------------
DROP TABLE IF EXISTS `scheduled_task`;
CREATE TABLE `scheduled_task`  (
  `TASK_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CLASS_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `METHOD_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REQ_PARAMS` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TASK_CRON` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TASK_STATE` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATE_TIME` datetime(0) DEFAULT NULL,
  `UPDATE_TIME` datetime(0) DEFAULT NULL,
  `REMARK` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`TASK_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `DICT_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DICT_TYPE_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DICT_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DICT_VALUE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`DICT_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '30', '病假', '1');
INSERT INTO `sys_dict` VALUES ('2', '30', '事假', '2');
INSERT INTO `sys_dict` VALUES ('3', '30', '产假', '3');
INSERT INTO `sys_dict` VALUES ('4', '10', '员工录入', '1001');
INSERT INTO `sys_dict` VALUES ('5', '10', '外部导入', '1002');
INSERT INTO `sys_dict` VALUES ('6', '20', '一般请假', '2001');
INSERT INTO `sys_dict` VALUES ('7', '20', '重要请假', '2002');
INSERT INTO `sys_dict` VALUES ('8', '10', '系统录入', '1003');
INSERT INTO `sys_dict` VALUES ('9', '20', '危机请假', '2003');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DICT_TYPE_CODE` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DICT_TYPE_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '30', '请假类型');
INSERT INTO `sys_dict_type` VALUES ('2', '10', '系统来源');
INSERT INTO `sys_dict_type` VALUES ('3', '20', '任务类型');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `USER_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `P_USER_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_PASS` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_PHOTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_NICK` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_SIGN` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user default password:123
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', NULL, 'zhangsan', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('2', '1', 'lisi', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('3', '2', 'wangwu', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('4', '3', 'sunqi', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('5', '3', 'zhaoliu', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('6', '5', 'ceshi_6', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('7', '6', 'ceshi_7', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES ('8', '7', 'ceshi_8', '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sort` int(0) DEFAULT NULL,
  `status` int(0) DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_at` datetime(0) DEFAULT NULL,
  `update_at` datetime(0) DEFAULT NULL,
  `delete_at` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('1', '0', '后台管理', 'radio', '', NULL, 1, 1, NULL, '2021-08-14 00:31:12', '2021-08-14 00:31:38', NULL);
INSERT INTO `system_menu` VALUES ('10', '2', '新建定时任务', 'layui-icon layui-icon-addition', '/page/addSchedule', NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('11', '2', '新建请假', 'layui-icon layui-icon-addition', '/page/addVacation', NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('2', '1', '系统管理', 'layui-icon layui-icon-location', '', NULL, 2, 1, NULL, '2021-08-14 00:31:42', NULL, NULL);
INSERT INTO `system_menu` VALUES ('3', '1', '流程管理', 'layui-icon layui-icon-print', '', NULL, 3, 1, NULL, '2021-08-14 00:31:42', NULL, NULL);
INSERT INTO `system_menu` VALUES ('4', '1', '审批管理', 'layui-icon layui-icon-snowflake', '', NULL, 4, 1, NULL, '2021-08-14 00:31:42', NULL, NULL);
INSERT INTO `system_menu` VALUES ('5', '3', '流程列表', 'layui-icon layui-icon-set', '/page/modelList', NULL, 5, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('6', '3', '流程规则', 'layui-icon layui-icon-radio', '/page/flowRule', NULL, 6, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('7', '4', '请假申请', 'layui-icon layui-icon-headset', '/page/vacation', NULL, 7, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('8', '4', '待办任务', 'layui-icon layui-icon-addition', '/page/myTask', NULL, 8, 1, NULL, NULL, NULL, NULL);
INSERT INTO `system_menu` VALUES ('9', '2', '新建工作流', 'layui-icon layui-icon-addition', '/page/addFlowRule', NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_flow_def
-- ----------------------------
DROP TABLE IF EXISTS `t_flow_def`;
CREATE TABLE `t_flow_def`  (
  `DEF_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FLOW_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `FLOW_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `FLOW_STATE` int(0) DEFAULT NULL,
  PRIMARY KEY (`DEF_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_flow_rule
-- ----------------------------
DROP TABLE IF EXISTS `t_flow_rule`;
CREATE TABLE `t_flow_rule`  (
  `RULE_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DEF_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `SYSTEM_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `BUSI_TYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RULE_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RULE_DESC` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`RULE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vacation_order
-- ----------------------------
DROP TABLE IF EXISTS `t_vacation_order`;
CREATE TABLE `t_vacation_order`  (
  `VACATION_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `START_TIME` datetime(0) DEFAULT NULL,
  `END_TIME` datetime(0) DEFAULT NULL,
  `VACATION_TYPE` int(0) DEFAULT NULL,
  `VACATION_CONTEXT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `VACATION_STATE` int(0) DEFAULT NULL,
  `SYSTEM_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `BUSI_TYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATE_TIME` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`VACATION_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tf_flow_main
-- ----------------------------
DROP TABLE IF EXISTS `tf_flow_main`;
CREATE TABLE `tf_flow_main`  (
  `FLOW_INST_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ORDER_NO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `FLOW_DEF_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `FLOW_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RULE_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `FLOW_STATE` int(0) DEFAULT NULL,
  `CREATE_TIME` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`FLOW_INST_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tl_process_log
-- ----------------------------
DROP TABLE IF EXISTS `tl_process_log`;
CREATE TABLE `tl_process_log`  (
  `LOG_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ORDER_NO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TASK_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TASK_KEY` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TASK_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `APPROV_STATU` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `OPER_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `OPER_VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATE_TIME` datetime(0) DEFAULT NULL,
  `REMARK` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`LOG_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
